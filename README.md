# Tag builder and parser

Author: [Peter Peto](http://petopeter.hu)


PHP based HTML and XML tag builder and parser

## Tech

PHP based

## Installation

Tag builder and parser requires [PHP](https://www.php.net/) v5.3.0+ to run.

You need to add this lines into your composer.json:
```sh
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/petop/tag-builder-and-parser.git"
    }
]
"require": {
    "petop/tag-builder-and-parser": "dev-master"
},
```

To install the defined dependencies for your project, run the `install` command.

```sh
$ php composer.phar update
$ php composer.phar install
```

## Usage

### Define odd tag
```php
$tag = new Tag('<hr/>');
```

### Define even tag
```php
$tag = new Tag('<div>');
```
or
```php
$tag = new Tag('div');
```

### Define tag with attribute
```php
$tag = new Tag('<hr style="width: 120px;"/>');
```
or
```php
$tag = new Tag('<div style="text-align:center;">');
```

### Echo tag string
```php
echo $tag;
```

### Add attribute
```php
$tag->AddAttribute('style', 'text-align:center;');
```

### Add content (string)
```php
$tag->addContent('www.petopeter.hu');
```

### Add (sub)tag **[only for even tag]**
```php
$tagBold = new Tag('b');
$tagBold->AddContent('Pető Péter');

$tagHyperlink = new Tag('a');
$tagHyperlink->AddAttribute('href', 'http://www.petopeter.hu');
$tagHyperlink->AddAttribute('target', '_blank');
$tagHyperlink->AddTag($tagBold);

echo $tagHyperlink;
```
`result: <a href="http://www.petopeter.hu" target="_blank"><b>Pető Péter</b></a>`

## Examples

### Define and echo odd tag
```php
$tag = new Tag('<hr/>');
echo $tag;
```
`result: <hr/>`

### Define and echo even tag
```php
$tag = new Tag('<div>');
echo $tag;
```
or
```php
$tag = new Tag('div');
echo $tag;
```
`result: <div></div>`

### Define and echo odd tag with attribute
```php
$tag = new Tag('<hr style="width: 120px;"/>');
echo $tag;
```
`result: <hr style="width: 120px;"/>`

### Define and echo odd tag (after add custom attribute)
```php
$tag = new Tag('<hr style="width: 120px;"/>');
$tag->AddAttribute('style', 'border: 1px solid;');
echo $tag;
```
`result: <hr style="width: 120px; border: 1px solid;"/>`

### Define and echo even tag (after add custom attribute and content)
```php
$tag = new Tag('a');
$tag->AddAttribute('href', 'http://www.petopeter.hu');
$tag->AddAttribute('target', '_blank');
$tag->AddContent('www.petopeter.hu');
echo $tag;
```
`result: <a href="http://www.petopeter.hu" target="_blank">www.petopeter.hu</a>`

### Div with contents and subtags
```php
$tagBold = new Tag('b');
$tagBold->AddContent('Pető Péter');

$tagHyperlink = new Tag('a');
$tagHyperlink->AddAttribute('href', 'http://www.petopeter.hu');
$tagHyperlink->AddAttribute('target', '_blank');
$tagHyperlink->AddTag($tagBold);

$tagDiv = new Tag('div');
$tagDiv->AddAttribute('style', 'text-align:center;');
$tagDiv->addContent('Author: ');
$tagDiv->addTag($tagHyperlink);
$tagDiv->addContent('.');
```
`result: <div style="text-align:center;">Author: <a href="http://www.petopeter.hu" target="_blank"><b>Pető Péter</b></a>.</div>`

## Licence Information
CC-BY-NC-SA
- ## You are free to:
  - **Share** — copy and redistribute the material in any medium or format
  - **Adapt** — remix, transform, and build upon the material
  > The licensor cannot revoke these freedoms as long as you follow the license terms.

- ## Under the following terms:
  - **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. 
  - **NonCommercial** — You may not use the material for commercial purposes.
  - **ShareAlike** — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
  - **No additional restrictions** — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
  > Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.