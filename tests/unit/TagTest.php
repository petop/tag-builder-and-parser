<?php
declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;
use PeterPeto\Tag;

class TagTest extends TestCase
{
    /**
     * @expectedException PHPUnit\Framework\Error\Error
     */
    public function testOddTagToString()
    {
        $tag = new Tag('<hr/>');

        $this->assertEquals($tag->__toString(), '<hr/>');
    }

    public function testOddTagWithAttributeToString()
    {
        $tag = new Tag('<hr width="120px"/>');

        $this->assertEquals($tag->__toString(), '<hr width="120px"/>');
    }

    public function testOddTagWithAttribute2ToString()
    {
        $tag = new Tag('<hr style="width: 120px;"/>');
        $tag->AddAttribute('style', 'border: 1px solid;');

        $assert = '<hr style="width: 120px; border: 1px solid;"/>';
        $this->assertEquals($tag->__toString(), $assert);
    }

    public function testEvenTagToSTring()
    {
        $tag = new Tag('a');

        $assert = '<a></a>';
        $this->assertEquals($tag->__toString(), $assert);
    }

    public function testEvenTagWithAttributeToString()
    {
        $tag = new Tag('a');
        $tag->AddAttribute('href', 'http://www.petopeter.hu');

        $assert = '<a href="http://www.petopeter.hu"></a>';
        $this->assertEquals($tag->__toString(), $assert);
    }

    public function testEvenTagWithAttributesToString()
    {
        $tag = new Tag('a');
        $tag->AddAttribute('href', 'http://www.petopeter.hu');
        $tag->AddAttribute('target', '_blank');

        $assert = '<a href="http://www.petopeter.hu" target="_blank"></a>';
        $this->assertEquals($tag->__toString(), $assert);
    }

    public function testEvenTagWithAttributesAndContentToString()
    {
        $tag = new Tag('a');
        $tag->AddAttribute('href', 'http://www.petopeter.hu');
        $tag->AddAttribute('target', '_blank');
        $tag->AddContent('www.petopeter.hu');

        $assert = '<a href="http://www.petopeter.hu" target="_blank">www.petopeter.hu</a>';
        $this->assertEquals($tag->__toString(), $assert);
    }

    public function testEvenTagWithAttributesAndTagToString()
    {
        $tag = new Tag('a');
        $tag->AddAttribute('href', 'http://www.petopeter.hu');
        $tag->AddAttribute('target', '_blank');

        $subTag = new Tag('b');
        $subTag->AddContent('www.petopeter.hu');
        $tag->AddTag($subTag);

        $assert = '<a href="http://www.petopeter.hu" target="_blank"><b>www.petopeter.hu</b></a>';
        $this->assertEquals($tag->__toString(), $assert);
    }

    public function testEvenTagWithAttributesAndTagAndContentToString()
    {
        $tagBold = new Tag('b');
        $tagBold->AddContent('Pető Péter');
        
        $tagHyperlink = new Tag('a');
        $tagHyperlink->AddAttribute('href', 'http://www.petopeter.hu');
        $tagHyperlink->AddAttribute('target', '_blank');
        $tagHyperlink->AddTag($tagBold);

        $tagDiv = new Tag('div');
        $tagDiv->AddAttribute('style', 'text-align:center;');
        $tagDiv->addContent('Author: ');
        $tagDiv->addTag($tagHyperlink);
        $tagDiv->addContent('.');

        $assert = '<div style="text-align:center;">Author: <a href="http://www.petopeter.hu" target="_blank"><b>Pető Péter</b></a>.</div>';
        $this->assertEquals($tagDiv->__toString(), $assert);
    }

    public function testEvenTagWithOddTagToString()
    {
        $tagSz = new Tag('<w:sz w:val="72"/>');
        
        $tagB = new Tag('<w:b/>');

        $tagPr = new Tag('w:rPr');
        $tagPr->addTag($tagB);
        $tagPr->addTag($tagSz);

        $assert = '<w:rPr><w:b/><w:sz w:val="72"/></w:rPr>';
        $this->assertEquals($tagPr->__toString(), $assert);
    }
}
