<?php
namespace PeterPeto;

class Tag {
    private $tagName;
    private $odd;
    private $php;
    private $attributes = [];
    private $content = [];

    public function __construct($tagString)
    {
        $this->isOddTag($tagString);
        $tagString = rtrim(rtrim(ltrim($tagString, '<'), '/>'), '>');
        $firstSpace = strpos($tagString, ' ');

        if($firstSpace === FALSE)
        {
            $this->tagName = $tagString;
            return;
        }

        $this->tagName = substr($tagString, 0, $firstSpace);

        preg_match_all('/([\w,:]*="([^"]*)")/i', $tagString, $attributes, PREG_PATTERN_ORDER);
        $attributes = $attributes[0];

        while(($attribute = array_shift($attributes)) !== NULL)
        {
            list($name, $property) = explode('=', $attribute);
            $property = trim($property, '"');
            $this->addAttribute($name, $property);
        }
    }

    public function addAttribute(string $name, string $value)
    {
        if(array_key_exists($name, $this->attributes))
        {
            if(!in_array($value, $this->attributes[$name]))
            {
                array_push($this->attributes[$name], $value);
            }
            return;
        }

        $this->attributes[$name] = [$value];
    }

    public function addContent(string $content)
    {
        if($this->odd)
        {
            return -1;
        }
        
        array_push($this->content, $content);
    }

    public function addTag(Tag $tag)
    {
        if($this->odd)
        {
            return -1;
        }
            
        array_push($this->content, $tag);
    }

    public function isPHP()
    {
        return $this->php;
    }

    public function isOdd()
    {
        return $this->odd;
    }

    public function __toString()
    {
        $string = "<" . $this->tagName;
        foreach($this->attributes as $name => $properties)
        {
            if(!empty($properties))
            {
                $propertyString = implode(' ', $properties);
                $string .= " {$name}=\"{$propertyString}\"";
            }
        }

        if($this->odd)
        {
            $string .= $this->php ? '>' : '/>';
            return $string;
        }

        $string .= ">";

        foreach($this->content as $content)
        {
            $string .= gettype($content) === "string" ? $content : $content->__toString();
        }

        $string .= "</{$this->tagName}>";

        return $string;
    }

    public function getName()
    {
        return $this->tagName;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function clearAttributes()
    {
        return $this->attributes = [];
    }

    public function clearContent()
    {
        $this->content = [];
    }

    private function isOddTag($tagString)
    {
        $beforeLastChar = $tagString[strlen($tagString) - 2];
        $this->php = $beforeLastChar == '?';
        $this->odd = $this->php || $beforeLastChar == '/';
    }
}